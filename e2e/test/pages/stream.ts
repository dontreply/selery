// stream.ts
import type { Page, Locator } from '@playwright/test';

export class Stream {
    readonly page: Page;
    constructor(page: Page) {
        this.page = page;
    }

    get buttonStart(): Locator {
        return this.page.locator('button#start');
    }
    get buttonStreamset(): Locator {
        return this.page.locator('button#streamset');
    }
    get buttonWatch(): Locator {
        return this.page.locator('button#watch');
    }
    get listStreamSet(): Locator {
        return this.page.locator('ul#streamslist .dropdown-item'); // unordered list
    }
}

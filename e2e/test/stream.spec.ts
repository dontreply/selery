// stream.spec.ts
import type { WebSocket } from '@playwright/test';

import { test } from './support/fixtures';
import { Stream } from './pages/stream';
import { randomIntBetween, randomItem } from './support/util';

for (let i = 0; i < 3; i++) {
    test(`Stream ${i}`, async ({ page }) => {
        page.on('websocket', async (websocket: WebSocket) => {
            let label = { app: `user`, level: 'error' };
            console.log(`WebSocket connected: ${websocket.url()}`);

            websocket.on('framereceived', data => {
                const payload = data.payload;
                console.log('WebSocket framereceived', payload);
            });

            websocket.on('framesent', data => {
                const payload = data.payload;
                console.log('WebSocket framesent', payload);
            });

            websocket.on('close', () => {
                console.log(`WebSocket closed`);
            });

            websocket.on('socketerror', error => {
                console.log('WebSocket Error', JSON.stringify(error));
            });
        });

        const stream = new Stream(page);
        await page.goto('https://janus.conf.meetecho.com/streamingtest.html');
        await stream.buttonStart.click();
        await stream.buttonStreamset.click(); // open dropdown
        const availableStreams = await stream.listStreamSet.all(); // get all the available stream items
        await randomItem(availableStreams).click(); // select a random item
        await stream.buttonWatch.click(); // start the stream
        await page.waitForTimeout(randomIntBetween(10_000, 30_000)); // watch time
        await stream.buttonWatch.click(); // stop the stream
        await page.waitForTimeout(randomIntBetween(500, 1500)); // linger time
    });
}

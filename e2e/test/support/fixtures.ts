// fixtures.ts
import { test as base } from '@playwright/test';

export const test = base.extend<{}, { workerId: number; uuid: string }>({
    workerId: [
        async ({}, use) => {
            await use(test.info().parallelIndex);
        },
        { scope: 'worker' },
    ],
    uuid: [
        async ({}, use) => {
            await use(crypto.randomUUID() as string);
        },
        { scope: 'worker' },
    ],
});

export const expect = test.expect;

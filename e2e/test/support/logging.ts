// logging.ts
import { APIResponse, APIRequestContext } from 'playwright-core';
const lokiURL = process.env.LOKI_LOG_OUTPUT || '';

export async function log(
    request: APIRequestContext,
    label: { [key: string]: string },
    ...data: any[]
): Promise<APIResponse> {
    const currentMillis = String(Date.now() * 1e6); // epoch 13-numbers
    const payload = {
        streams: [
            {
                stream: label,
                values: data.map(x => [currentMillis, x]), //  [ [ "epoch_ms", "data" ], ... ]
            },
        ],
    };
    return await request.post(lokiURL, {
        ignoreHTTPSErrors: true,
        failOnStatusCode: false,
        headers: {
            'Content-Type': 'application/json',
        },
        data: payload,
    });
}

// export class Log {
//     _stdout;
//     _stderr;

//     constructor(request: APIRequestContext, ) {
//         this._stdout = process.stdout.write.bind(process.stdout);
//         this._stderr = process.stderr.write.bind(process.stderr);

//         process.stdout.write = (chunk, encoding, callback) => {
//             if (typeof chunk === 'string') {
//               output += chunk;
//             }
//             return this._stdout(chunk, encoding, callback);
//            };

//         process.stderr.write = (chunk, encoding, callback) => {
//             if (typeof chunk === 'string') {
//               errorOutput += chunk;
//             }
//         return this._stderr(chunk, encoding, callback);
//         };
//     }

// }

// let output = '';
// let errorOutput = '';

// const originalStdoutWrite =
// const originalStderrWrite =

// process.stdout.write = (chunk, encoding, callback) => {
//  if (typeof chunk === 'string') {
//    output += chunk;
//  }
//  return originalStdoutWrite(chunk, encoding, callback);
// };

// process.stderr.write = (chunk, encoding, callback) => {
//  if (typeof chunk === 'string') {
//    errorOutput += chunk;
//  }
//  return originalStderrWrite(chunk, encoding, callback);
// };

// playwright.config.ts
import { defineConfig, devices } from '@playwright/test';
import type { PlaywrightTestConfig } from '@playwright/test';

const second = 1_000;
const minute = 60 * second;
const hour = 60 * minute;

const config: PlaywrightTestConfig = {
    testDir: './test',
    outputDir: './test-results',
    forbidOnly: true,
    retries: 0,

    fullyParallel: true,
    workers: 255,
    reporter: [['blob', { outputDir: './test-results/blob-results' }]],

    /* Maximum time one test can run for. */
    timeout: 1 * hour + 30 * minute,
    expect: {
        /* Default timeout for async expect matchers in milliseconds, defaults to 5000ms. */
        timeout: 0,
    },
    use: {
        /* Timeout for each navigation action in milliseconds. Defaults to 0 (no timeout) */
        actionTimeout: 10000,
        trace: 'on',
        headless: true,
    },
    projects: [
        {
            name: 'chromium',
            use: {
                ...devices['Desktop Chrome'], // includes user agent
                viewport: { width: 1280, height: 720 },
                launchOptions: {
                    args: [
                        '--incognito',
                        '--disable-background-networking',
                        '--disable-extensions',
                        '--disable-gpu',
                        '--disable-dev-shm-usage',
                        '--disable-software-rasterizer',
                    ],
                },
            },
        },
        // {
        //     name: 'node-edge',
        //     use: {
        //         ...devices['Desktop Edge'], // includes user agent
        //         channel: 'msedge'  // important!
        //     },
        // },
    ],
};

export default defineConfig(config);

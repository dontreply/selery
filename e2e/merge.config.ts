// merge.config.ts

/**
 * Usage:
 *  commandline:
 *      yarn playwright merge-reports --config=merge.config.ts ./test-results/blob-results
 *  or:
 *      yarn pw-merge-reports
 */
export default {
    testDir: './e2e/test',
    reporter: [
        ['html', { open: 'never', outputFolder: './playwright-report' }],
        ['json', { outputFile: `./test-results/json-results/${+new Date()}.json` }],
        ['junit', { outputFile: `./test-results/junit-results/${+new Date()}.xml` }],
    ],
};
